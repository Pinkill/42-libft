/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 19:23:24 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/12 20:46:14 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_length(int n)
{
	int	count;

	count = 0;
	if (n > 0)
		while (n > 0)
		{
			count++;
			n /= 10;
		}
	else
		while (n < 0)
		{
			count++;
			n /= 10;
		}
	return (count);
}

char		*ft_itoa(int n)
{
	char	*str;
	int		len;

	len = ft_length(n);
	if (n <= 0)
		len++;
	if (!(str = (char*)malloc(len + 1)))
		return (0);
	str[len] = '\0';
	if (n < 0)
		str[0] = '-';
	if (n == 0)
		str[0] = '0';
	while (len-- > 0)
	{
		if (n < 0)
			str[len] = ((n % 10) * -1) + '0';
		else if (n > 0)
			str[len] = (n % 10) + '0';
		n /= 10;
	}
	return (str);
}
