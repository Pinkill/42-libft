/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/10 19:38:50 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/13 16:48:45 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(const char *s, size_t start, size_t len)
{
	char	*dest;

	if (s == 0)
		return (0);
	if (start > ft_strlen(s))
	{
		if (!(dest = (char*)malloc(1)))
			return (0);
		dest[0] = '\0';
	}
	else
	{
		if (!(dest = (char*)malloc(len + 1)))
			return (0);
		ft_memcpy(dest, s + start, len);
		dest[len] = '\0';
	}
	return (dest);
}
