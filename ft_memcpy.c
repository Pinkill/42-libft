/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 12:06:34 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/09 17:25:41 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char	*ptrdest;
	const char		*ptrsrc;

	ptrdest = dst;
	ptrsrc = src;
	if (dst == 0 && src == 0)
		return (dst);
	while (n-- > 0)
		*ptrdest++ = *ptrsrc++;
	return (dst);
}
