/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/10 18:42:01 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/10 19:22:48 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	size_t *ptr;
	size_t bytes;

	if (count == 0 || size == 0)
	{
		count = 1;
		size = 1;
	}
	bytes = size * count;
	ptr = malloc(bytes);
	if (!ptr)
		return (0);
	ft_bzero(ptr, bytes);
	return (ptr);
}
