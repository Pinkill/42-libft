/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:05:41 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/14 12:08:35 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list			*ft_lstnew(void *content)
{
	t_list			*begin;

	if (!(begin = malloc(sizeof(t_list))))
		return (0);
	begin->content = content;
	begin->next = 0;
	return (begin);
}
