/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/10 19:55:31 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/13 18:48:07 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(const char *s1, const char *s2)
{
	char	*dest;
	size_t	sizedest;
	size_t	sizesrc;

	if (s1 == 0 || s2 == 0)
		return (0);
	sizedest = ft_strlen(s1);
	sizesrc = ft_strlen(s2);
	dest = (char*)malloc(sizedest + sizesrc + 1);
	if (!dest)
		return (0);
	ft_memcpy(dest, s1, sizedest);
	ft_memcpy(dest + sizedest, s2, sizesrc);
	dest[sizedest + sizesrc] = '\0';
	return (dest);
}
