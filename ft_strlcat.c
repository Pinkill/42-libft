/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 18:01:39 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/15 17:32:22 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t dstsize)
{
	size_t	sizedest;
	size_t	i;
	size_t	sizesrc;

	sizedest = ft_strlen(dest);
	sizesrc = ft_strlen(src);
	i = 0;
	if (dstsize <= sizedest)
		return (sizesrc + dstsize);
	while (src[i] != '\0' && sizedest + i + 1 < dstsize)
	{
		dest[sizedest + i] = src[i];
		i++;
	}
	dest[sizedest + i] = '\0';
	return (sizesrc + sizedest);
}
