/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/11 21:25:27 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/13 16:06:12 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static const char	*ft_next_set(const char *s, char c)
{
	while (*s != '\0' && *s == c)
		s++;
	return (s);
}

static size_t		ft_count_splits(const char *s, char c)
{
	size_t	count;
	int		flag;

	count = 0;
	flag = 0;
	if (s == 0 || c == 0)
		return (0);
	while (*s != '\0')
	{
		if (*s != c && flag == 0)
		{
			count++;
			flag = 1;
		}
		if (*s++ == c)
			flag = 0;
	}
	return (count);
}

static size_t		ft_count_words(const char *s, char c)
{
	size_t	count;

	count = 0;
	while (*s != '\0' && *s++ != c)
		count++;
	return (count);
}

static void			*ft_free_all(char **result, size_t n)
{
	while (n-- >= 0)
		free(result[n]);
	free(result);
	return (0);
}

char				**ft_split(const char *s, char c)
{
	char	**result;
	size_t	i;
	size_t	j;
	size_t	splits;
	size_t	len;

	i = 0;
	splits = ft_count_splits(s, c);
	if (!(result = (char**)malloc(sizeof(char**) * (splits + 1))))
		return (0);
	while (i < splits)
	{
		j = 0;
		s = ft_next_set(s, c);
		len = ft_count_words(s, c);
		if (!(result[i] = (char*)malloc(sizeof(char*) * (len + 1)))
			&& ft_free_all(result, i))
			return (0);
		while (len-- > 0)
			result[i][j++] = *s++;
		result[i++][j] = '\0';
	}
	result[i] = 0;
	return (result);
}
