# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/07 15:39:40 by mknezevi          #+#    #+#              #
#    Updated: 2019/11/15 17:48:23 by mknezevi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC = ft_atoi.c ft_bzero.c ft_calloc.c ft_isalnum.c ft_isalpha.c ft_isascii.c \
ft_isdigit.c ft_isprint.c ft_isspace.c ft_itoa.c ft_memccpy.c ft_memchr.c \
ft_memcmp.c ft_memcpy.c ft_memmove.c ft_memset.c ft_putchar_fd.c \
ft_putendl_fd.c ft_putnbr_fd.c ft_putstr_fd.c ft_split.c ft_strchr.c \
ft_strdup.c ft_strjoin.c ft_strlcpy.c ft_strlen.c ft_strlcat.c ft_strmapi.c \
ft_strncmp.c ft_strnstr.c ft_strrchr.c ft_strtrim.c ft_substr.c ft_tolower.c \
ft_toupper.c 

BONUS = ft_lstadd_back_bonus.c ft_lstadd_front_bonus.c ft_lstclear_bonus.c \
ft_lstdelone_bonus.c ft_lstiter_bonus.c ft_lstlast_bonus.c ft_lstmap_bonus.c \
ft_lstnew_bonus.c ft_lstsize_bonus.c

SRCS = ${addprefix ${PRE}, ${SRC}}

OBJS = ${SRCS:.c=.o}

BONUS_OBJS = ${BONUS:.c=.o}

PRE = ./

HEAD = ./

NAME = libft.a

AR = ar rc

RAN = ranlib

GCC = gcc

CFLAG = -Wall -Wextra -Werror

all:	$(NAME)

%.o: %.c
	${GCC} ${CFLAG} -c -I ${HEAD} $< -o ${<:.c=.o}

$(NAME): ${OBJS}
	${AR} ${NAME} ${OBJS}
	${RAN} ${NAME}

bonus:	${OBJS} ${BONUS_OBJS}
	${AR} ${NAME} ${BONUS} ${OBJS}
	${RAN} ${NAME}

clean:
	rm -f ${OBJS}
	rm -f ${BONUS_OBJS}

fclean:	clean
	rm -f ${NAME}

re: 	fclean all

.PHONY:		all clean fclean re