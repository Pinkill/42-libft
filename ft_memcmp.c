/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 17:26:27 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/10 15:12:35 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char *ptrs1;
	const unsigned char *ptrs2;

	ptrs1 = (const unsigned char*)s1;
	ptrs2 = (const unsigned char*)s2;
	while (n-- > 0)
	{
		if (*ptrs1 != *ptrs2)
			return ((unsigned char)*ptrs1 - (unsigned char)*ptrs2);
		ptrs1++;
		ptrs2++;
	}
	return (0);
}
