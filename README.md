# 42 Libft Library

This is a starting project in the 42 study program. Libft is a static C library consisting of replicate functions from library string.h and some custom ones (print out functions and linked list modifying functions).

To see the documentation for all the functions and how they work, check "en.subject.pdf" file.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

For Linux or Mac:
```
- git (optional, I guess)
- gcc
- ar
- ranlib
```

### Installing

Getting the project:

```
git clone https://gitlab.com/Pinkill/42-libft libft
```

#### Building the static library:

```
make all
```

#### Or (to include bonus):

```
make bonus
```

#### Cleaning, rebuilding the project:

Removes library, but not the objects

```
make clean
```

Cleans whole project

```
make fclean
```

Cleans and rebuilds whole project (without bonus)

```
make re
```

Good luck!