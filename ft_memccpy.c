/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 14:49:42 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/10 16:18:22 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char		*ptrdst;
	const unsigned char	*ptrsrc;

	if (n > 0)
	{
		ptrdst = dst;
		ptrsrc = src;
		while (n-- > 0)
		{
			*ptrdst++ = *ptrsrc;
			if (*ptrsrc++ == (unsigned char)c)
				return (ptrdst);
		}
	}
	return (0);
}
