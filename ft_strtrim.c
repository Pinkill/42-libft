/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/10 20:39:35 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/11 21:23:49 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_checkset(const char *s1, const char *set)
{
	size_t	j;
	size_t	count;

	count = 0;
	j = 0;
	while (set[j] != '\0')
	{
		if (*s1 == set[j++])
		{
			s1++;
			count += 1;
			j = 0;
		}
	}
	return (count);
}

static size_t	ft_checkendset(const char *s1, const char *set, size_t i)
{
	size_t	j;
	size_t	count;

	j = 0;
	count = 0;
	while (set[j] != '\0')
	{
		if (s1[i] == set[j++])
		{
			i--;
			count++;
			j = 0;
		}
	}
	return (count);
}

char			*ft_strtrim(const char *s1, const char *set)
{
	char	*result;
	size_t	ssize;
	size_t	start;
	size_t	end;

	if (s1 == 0 || set == 0)
		return (0);
	start = 0;
	end = 0;
	ssize = ft_strlen(s1);
	start = ft_checkset(s1, set);
	if (start < ssize)
		end = ft_checkendset(s1, set, ssize - 1);
	result = (char*)malloc(ssize - start - end + 1);
	if (!result)
		return (0);
	ft_memcpy(result, s1 + start, ssize - start - end);
	result[ssize - start - end] = '\0';
	return (result);
}
