/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 18:34:39 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/15 16:09:07 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*head;
	t_list	*new;

	if (!lst)
		return (0);
	if (!(new = ft_lstnew(f(lst->content))))
		return (0);
	head = new;
	while (lst->next)
	{
		if (!(new->next = ft_lstnew(f(lst->next->content))))
		{
			ft_lstclear(&head, del);
			return (0);
		}
		new = new->next;
		lst = lst->next;
	}
	return (head);
}
