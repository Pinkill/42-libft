/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 21:34:58 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/13 14:21:48 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_print_nb(int n, int fd)
{
	int	c;

	if (n > 0)
	{
		ft_print_nb(n / 10, fd);
		c = (n % 10) + 48;
		write(fd, &c, 1);
	}
}

void			ft_putnbr_fd(int n, int fd)
{
	unsigned int	a;

	if (fd < 0 || fd > 2)
		return ;
	if (n == -2147483648)
	{
		write(fd, "-2147483648", 11);
		return ;
	}
	if (n == 0)
		write(fd, "0", 1);
	else
	{
		if (n < 0)
		{
			write(fd, "-", 1);
			a = n * -1;
		}
		else
			a = n;
		ft_print_nb(a, fd);
	}
}
