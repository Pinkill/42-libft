/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 17:46:38 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/14 18:26:53 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list	*tmp;
	t_list	*head;

	head = *lst;
	if (lst == 0)
		return ;
	if (del)
	{
		while (head)
		{
			tmp = head;
			head = head->next;
			del(tmp->content);
			free(tmp);
		}
	}
	*lst = 0;
}
