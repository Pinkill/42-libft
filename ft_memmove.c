/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 12:50:44 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/09 17:57:52 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char	*ptrdest;
	const char		*ptrsrc;

	ptrdest = dst;
	ptrsrc = src;
	if (dst == 0 && src == 0)
		return (dst);
	if (ptrdest > (unsigned char*)ptrsrc)
	{
		while (len-- > 0)
		{
			ptrdest[len] = ptrsrc[len];
		}
	}
	else
	{
		while (len-- > 0)
			*ptrdest++ = *ptrsrc++;
	}
	return (dst);
}
